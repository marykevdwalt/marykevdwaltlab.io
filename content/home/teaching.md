+++
date = "2016-04-20T00:00:00"
draft = false
title = "teaching"
subtitle = ""
section_id = 10
weight = 10
+++

In Fall 2020 at Westmont College, I am teaching:

- MA-108: Mathematical Analysis
- MA-005: Intoduction to Statistics

<!-- Courses I have previously taught (as primary instructor):

- Calculus I (Westmont College, Fall 2017, Spring 2018)
- Calculus II (Westmont College, Spring 2018)
- Introduction to Subdivision Methods (Westmont College, Spring 2018)
- Introduction to Numerical Mathematics (Vanderbilt University, Spring 2017)
- Linear Algebra (Vanderbilt University, Fall 2016, Spring 2017, Summer 2017)
- Accelerated Single-Variable Calculus II (Vanderbilt University, Summer 2016)
- Differential Equations and Linear Algebra (Vanderbilt University, Spring 2016)
- Trigonometry (University of Missouri-St. Louis, Spring 2014, Fall 2014, Spring 2015)
- Basic Probability and Statistics (University of Missouri-St. Louis, Spring 2014)
- Intermediate Algebra (University of Missouri-St. Louis, Fall 2013)
- Beginning Algebra (University of Missouri-St. Louis, Fall 2013)
- Calculus I (Stellenbosch University, South Africa, Fall 2012)

Courses for which I have been a Teaching Assistant:

- Calculus I (Stellenbosch University, 2009, 2011, 2012)
- Mathematics for the Biological Sciences  (Stellenbosch University, 2009, 2010, 2011, 2012)
- Introductory Mathematics (Stellenbosch University, 2010)
- Introductory Differential and Integral Calculus (Stellenbosch University, 2010, 2011)
- Further Differential and Integral Calculus  (Stellenbosch University, 2010, 2011)
- Differential Equations and Linear Algebra  (Stellenbosch University, 2011, 2012)
- Series, Partial Differential Equations and Fourier Transform (Stellenbosch University, 2010, 2011)
- Linear Programming (Stellenbosch University, 2009)
- Nonlinear Optimization (Stellenbosch University, 2009) -->

<!--[Here](/teaching_statement.pdf) is my teaching statement.-->
