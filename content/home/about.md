+++
date = "2016-04-20T00:00:00"
draft = false
title = "about"
section_id = 0
weight = 0
+++

# Biography

My name is Maria, but my family and friends call me Maryke (a pet form of Maria or Mary). (Maryke is pronounced muh - RAY - kuh.)

I was born in South Africa. I grew up in a town called Paarl (which means 'pearl' -- the town lies at the foot of Paarl Mountain, a granite rock that is supposed to shimmer like a pearl when the sun comes out after the rain). Paarl is about 45 minutes' drive outside Cape Town.

My first language is Afrikaans, a language that developed from Dutch (South Africa used to be a Dutch colony).

Apart from thinking and talking about mathematics, I love to cook and bake (especially new recipes), play the violin and viola, and go for walks and visit coffee shops with my husband and best friend, [Tjaart](http://www.tjaart.co.za/), and our daughter, Lisa.
