+++
abstract = "We develop a local polynomial spline interpolation scheme for arbitrary spline order on bounded intervals. Our method's local formulation, effective boundary considerations and optimal interpolation error rate make it particularly useful for real-time implementation in real-world applications."
abstract_short = ""
authors = ["M.D. van der Walt"]
date = "2016-01-01"
image = ""
image_preview = ""
math = true
publication = "In *Applied Mathematical Sciences*"
publication_short = "In *AMS*"
selected = false
title = "Real-time, local spline interpolation schemes on bounded intervals"
url_code = "https://github.com/marykevdwalt/code_blending_operator"
url_dataset = ""
url_pdf = "http://www.m-hikari.com/ams/ams-2016/ams-5-8-2016/p/vanderwaltAMS5-8-2016.pdf"
url_project = ""
url_slides = ""
url_video = ""




+++
